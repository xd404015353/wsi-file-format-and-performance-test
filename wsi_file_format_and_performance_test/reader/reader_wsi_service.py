import asyncio
import os
import time
from io import BytesIO
from uuid import NAMESPACE_URL, uuid5

import aiohttp
import docker
import requests
from PIL import Image

Image.MAX_IMAGE_PIXELS = None

from wsi_file_format_and_performance_test.models import (
    ImageSize,
    ImageSizeWithDepth,
    PixelSizeInMicrons,
)
from wsi_file_format_and_performance_test.reader_template import SlideReaderAsync


class SlideReaderWSIService(SlideReaderAsync):
    def __init__(self, filepath):
        self.slide_id = self._get_slide_id(filepath)
        self.client = docker.from_env()
        self.docker = f"registry.gitlab.com/empaia/services/wsi-service"
        self.client.images.pull(self.docker)
        self.data_folder = os.path.dirname(os.path.dirname(filepath))
        if "TESTDATA_PATH" in os.environ and "CI" in os.environ:
            self.data_folder = self.data_folder.replace(
                "/testdata", os.environ["TESTDATA_PATH"]
            )
        while "wsi-service" in self._get_running_containers_list():
            container = self.client.containers.get("wsi-service")
            container.remove(force=True)
            time.sleep(1)
        self.container = self.client.containers.run(
            self.docker,
            name="wsi-service",
            detach=True,
            ports={"8080": 8080},
            volumes={self.data_folder: {"bind": "/data", "mode": "ro"}},
            remove=True,
            environment=["WS_MAX_RETURNED_REGION_SIZE=2500000000"],
        )
        self._wait_for_service()
        r = requests.get(f"http://localhost:8080/v3/slides/{self.slide_id}/info")
        r.raise_for_status()
        self.info = r.json()
        connector = aiohttp.TCPConnector(force_close=True)
        self.session = aiohttp.ClientSession(connector=connector)

    def __del__(self):
        self.container.remove(force=True)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.session.close())

    @classmethod
    def get_supported_formats(self):
        return ["svs", "tiff", "mrxs"]

    @classmethod
    def get_reader_name(self):
        return f"wsi-service"

    def get_version(self):
        r = requests.get(f"http://localhost:8080/alive")
        r.raise_for_status()
        return r.json()["version"]

    def _get_image_size(self):
        return ImageSizeWithDepth(
            width=self.info["extent"]["x"], height=self.info["extent"]["y"], depth=1
        )

    def _get_tile_size(self):
        return ImageSize(
            width=self.info["tile_extent"]["x"], height=self.info["tile_extent"]["y"]
        )

    def _get_pixel_size(self):
        return PixelSizeInMicrons(
            x=0.001 * self.info["pixel_size_nm"]["x"],
            y=0.001 * self.info["pixel_size_nm"]["y"],
        )

    def _get_channels(self):
        return len(self.info["channels"])

    def _get_bits(self):
        return self.info["channel_depth"]

    def _get_levels(self):
        levels = []
        for level in self.info["levels"]:
            levels.append(
                ImageSize(width=level["extent"]["x"], height=level["extent"]["y"])
            )
        return levels

    def _get_label_size(self):
        label = self.get_label()
        image_size = None
        if label:
            image_size = ImageSize(width=label.width, height=label.height)
        return image_size

    def _get_macro_size(self):
        macro = self.get_macro()
        image_size = None
        if macro:
            image_size = ImageSize(width=macro.width, height=macro.height)
        return image_size

    def _get_thumbnail_size(self):
        thumbnail = self.get_macro()
        image_size = None
        if thumbnail:
            image_size = ImageSize(width=thumbnail.width, height=thumbnail.height)
        return image_size

    def get_macro(self):
        try:
            r = requests.get(
                f"http://localhost:8080/v3/slides/{self.slide_id}/macro/max_size/100/100?image_format=png",
                stream=True,
            )
            img = Image.open(r.raw)
        except:
            img = None
        return img

    def get_label(self):
        try:
            r = requests.get(
                f"http://localhost:8080/v3/slides/{self.slide_id}/label/max_size/100/100?image_format=png",
                stream=True,
            )
            img = Image.open(r.raw)
        except:
            img = None
        return img

    def get_thumbnail(self):
        try:
            r = requests.get(
                f"http://localhost:8080/v3/slides/{self.slide_id}/thumbnail/max_size/100/100?image_format=png",
                stream=True,
            )
            img = Image.open(r.raw)
        except:
            img = None
        return img

    async def get_tile(self, level, tile_x, tile_y):
        url = f"http://localhost:8080/v3/slides/{self.slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}?image_format=png"
        async with self.session.get(url) as r:
            tile = await r.read()
        return Image.open(BytesIO(tile))

    async def get_region(self, level, start_x, start_y, size_x, size_y):
        url = f"http://localhost:8080/v3/slides/{self.slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}?image_format=png"
        async with self.session.get(url) as r:
            region = await r.read()
        return Image.open(BytesIO(region))

    def _wait_for_service(self):
        for _ in range(60):
            try:
                r = requests.get("http://localhost:8080/alive")
                if r.json()["status"] == "ok":
                    response = requests.get(
                        "http://localhost:8080/refresh_local_mapper"
                    )
                    assert response.status_code == 200
                    break
            except requests.exceptions.RequestException:
                pass
            time.sleep(1)

    def _get_slide_id(self, filepath):
        return uuid5(NAMESPACE_URL, "slides" + os.path.basename(filepath)).hex

    def _get_running_containers_list(self):
        containers = self.client.containers.list()
        return [container.name for container in containers]
