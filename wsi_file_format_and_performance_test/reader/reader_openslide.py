import openslide
from PIL import Image

from wsi_file_format_and_performance_test.models import (
    ImageSize,
    ImageSizeWithDepth,
    PixelSizeInMicrons,
)
from wsi_file_format_and_performance_test.reader_template import SlideReader


class SlideReaderOpenslide(SlideReader):
    def __init__(self, filepath):
        self.slide = openslide.OpenSlide(filepath)

    @classmethod
    def get_supported_formats(self):
        return ["svs", "tiff", "mrxs"]

    @classmethod
    def get_reader_name(self):
        return "openslide"

    def get_version(self):
        return openslide.__version__

    def _get_image_size(self):
        return ImageSizeWithDepth(
            width=self.slide.dimensions[0], height=self.slide.dimensions[1], depth=1
        )

    def _get_tile_size(self):
        try:
            image_size = ImageSize(
                width=self.slide.properties["openslide.level[0].tile-width"],
                height=self.slide.properties["openslide.level[0].tile-height"],
            )
        except KeyError:
            image_size = ImageSize(width=256, height=256)
        return image_size

    def _get_pixel_size(self):
        pixel_size = None
        try:
            pixel_size = PixelSizeInMicrons(
                x=float(self.slide.properties[openslide.PROPERTY_NAME_MPP_X]),
                y=float(self.slide.properties[openslide.PROPERTY_NAME_MPP_Y]),
            )
        except KeyError:
            pixel_size = None
        return pixel_size

    def _get_channels(self):
        return 3

    def _get_bits(self):
        return 8

    def _get_levels(self):
        levels = []
        for dimensions in self.slide.level_dimensions:
            levels.append(ImageSize(width=dimensions[0], height=dimensions[1]))
        return levels

    def get_tile(self, level, tile_x, tile_y):
        tile_size = self._get_tile_size()
        return self.get_region(
            level,
            tile_x * tile_size.width,
            tile_y * tile_size.height,
            tile_size.width,
            tile_size.height,
        )

    def get_region(self, level, start_x, start_y, size_x, size_y):
        downsample_factor = self.slide.level_downsamples[level]
        level_0_location = (
            (int)(start_x * downsample_factor),
            (int)(start_y * downsample_factor),
        )
        return self._rgba_to_rgb(
            self.slide.read_region(level_0_location, level, (size_x, size_y))
        )

    def get_label(self):
        return self._get_associated_image("label")

    def get_macro(self):
        return self._get_associated_image("macro")

    def get_thumbnail(self):
        return self._get_associated_image("thumbnail")

    def _get_label_size(self):
        label = self.get_label()
        image_size = None
        if label:
            image_size = ImageSize(width=label.width, height=label.height)
        return image_size

    def _get_macro_size(self):
        macro = self.get_macro()
        image_size = None
        if macro:
            image_size = ImageSize(width=macro.width, height=macro.height)
        return image_size

    def _get_thumbnail_size(self):
        thumbnail = self.get_thumbnail()
        image_size = None
        if thumbnail:
            image_size = ImageSize(width=thumbnail.width, height=thumbnail.height)
        return image_size

    def _rgba_to_rgb(self, image_rgba):
        image_rgb = Image.new("RGB", image_rgba.size)
        image_rgb.paste(image_rgba, mask=image_rgba.split()[3])
        return image_rgb

    def _get_associated_image(self, associated_image_name):
        if associated_image_name not in self.slide.associated_images:
            return None
        associated_image_rgba = self.slide.associated_images[associated_image_name]
        return self._rgba_to_rgb(associated_image_rgba)
