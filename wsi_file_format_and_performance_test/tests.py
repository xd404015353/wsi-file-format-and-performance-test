import asyncio
import json
import os
import time
from concurrent.futures import ThreadPoolExecutor
from io import BytesIO
from random import shuffle
from typing import Any, Callable, Tuple

import numpy as np
from PIL import ImageStat

from wsi_file_format_and_performance_test.helpers import get_data_path
from wsi_file_format_and_performance_test.models import SlideTestResult


def test_slide_info(reader, slide_file, filepath):
    results_folder = os.path.join(get_data_path(), "results")
    os.makedirs(results_folder, exist_ok=True)
    comparison_result = slide_file.slideinfo.compare(reader.get_slide_info())
    result_file_path = os.path.join(
        results_folder, reader.get_reader_name() + "-" + os.path.basename(filepath)
    )
    comparison_result.save(result_file_path)
    reader.get_slide_info().save(result_file_path.replace(".json", "-info.json"))
    return result_file_path


def test_slide_data(reader, slide_file, result_file_path):
    # load and extend existing results
    with open(result_file_path, "r") as f:
        slide_test_result = SlideTestResult(**json.load(f))

    if slide_file.slidedata:
        # test tiles
        if len(slide_file.slidedata.image_tile_mean) > 0:
            slide_test_result.image_tile_mean = []
        for tile_data in slide_file.slidedata.image_tile_mean:
            try:
                tile = _get_tile(
                    reader.get_tile, tile_data.level, tile_data.tile_x, tile_data.tile_y
                )
                slide_test_result.image_tile_mean.append(
                    bool(tile_data.mean == np.mean(ImageStat.Stat(tile).mean))
                )
            except:
                slide_test_result.image_tile_mean.append(False)
        # test regions
        if len(slide_file.slidedata.image_region_mean) > 0:
            slide_test_result.image_region_mean = []
        for region_data in slide_file.slidedata.image_region_mean:
            try:
                region = _get_region(
                    reader.get_region,
                    region_data.level,
                    region_data.start_x,
                    region_data.start_x,
                    region_data.size_x,
                    region_data.size_y,
                )
                slide_test_result.image_region_mean.append(
                    bool(region_data.mean == np.mean(ImageStat.Stat(region).mean))
                )
            except:
                slide_test_result.image_region_mean.append(False)
        # test macro
        if slide_file.slidedata.image_macro_mean:
            try:
                macro = reader.get_macro()
                slide_test_result.image_macro_mean = bool(
                    slide_file.slidedata.image_macro_mean
                    == np.mean(ImageStat.Stat(macro).mean)
                )
            except:
                slide_test_result.image_macro_mean = False
        # test label
        if slide_file.slidedata.image_label_mean:
            try:
                label = reader.get_label()
                slide_test_result.image_label_mean = bool(
                    slide_file.slidedata.image_label_mean
                    == np.mean(ImageStat.Stat(label).mean)
                )
            except:
                slide_test_result.image_label_mean = False
        # test thumbnail
        if slide_file.slidedata.image_thumbnail_mean:
            try:
                thumbnail = reader.get_thumbnail()
                slide_test_result.image_thumbnail_mean = bool(
                    slide_file.slidedata.image_thumbnail_mean
                    == np.mean(ImageStat.Stat(thumbnail).mean)
                )
            except:
                slide_test_result.image_thumbnail_mean = False
    slide_test_result.save(result_file_path)


def test_performance(reader, result_file_path):
    # load and extend existing results
    with open(result_file_path, "r") as f:
        slide_test_result = SlideTestResult(**json.load(f))
    # tile
    level_index = _get_level_for_performance_testing(reader)
    random_tiles = _get_tile_index_list(reader, level_index)
    start_time = time.time()
    tiles = _get_async(reader.get_tile, random_tiles)
    tile_size = reader._get_tile_size()
    elapsed_time = time.time() - start_time
    pixel_count = len(random_tiles) * tile_size.width * tile_size.height
    slide_test_result.tile_performance = int(pixel_count / elapsed_time)
    slide_test_result.tile_performance_io = _get_io_performance_tiles(
        tiles, elapsed_time
    )
    # region
    levels = reader._get_levels()
    width = levels[level_index].width
    height = levels[level_index].height
    start_time = time.time()
    region = _get_region(reader.get_region, level_index, 0, 0, width, height)
    elapsed_time = time.time() - start_time
    pixel_count = width * height
    slide_test_result.region_performance = int(pixel_count / elapsed_time)
    slide_test_result.region_performance_io = _get_io_performance_region(
        region, elapsed_time
    )
    # save performance results
    slide_test_result.save(result_file_path)


def _get_tile_index_list(reader, level_index):
    tile_size = reader._get_tile_size()
    levels = reader._get_levels()
    tile_count_x = int(levels[level_index].width / tile_size.width)
    tile_count_y = int(levels[level_index].height / tile_size.height)
    tile_index_list = []
    for i in range(tile_count_x):
        for j in range(tile_count_y):
            tile_index_list.append((level_index, i, j))
    shuffle(tile_index_list)
    return tile_index_list


def _get_level_for_performance_testing(reader, min_tile_count=1000):
    tile_size = reader._get_tile_size()
    levels = reader._get_levels()
    level_index_for_performance_testing = 0
    for i, level in enumerate(levels):
        tile_count_x = int(level.width / tile_size.width)
        tile_count_y = int(level.height / tile_size.height)
        tile_count = tile_count_x * tile_count_y
        if tile_count > min_tile_count:
            level_index_for_performance_testing = i
    return level_index_for_performance_testing


def _get_io_performance_tiles(tiles, elapsed_time):
    transfered_data = 0
    for tile in tiles:
        io = BytesIO()
        format = tile.format
        if format is None:
            format = "JPEG"
        tile.save(io, format)
        transfered_data += io.tell()
    return int((transfered_data / 1024) / elapsed_time)  # KB/s


def _get_io_performance_region(region, elapsed_time):
    io = BytesIO()
    format = region.format
    if format is None:
        format = "JPEG"
    region.save(io, format)
    return int((io.tell() / 1024) / elapsed_time)  # KB/s


def _get_tile(tile_function, level, x, y):
    return _get_async(tile_function, [(level, x, y)])[0]


def _get_region(region_function, level, start_x, start_y, size_x, size_y):
    return _get_async(region_function, [(level, start_x, start_y, size_x, size_y)])[0]


def _get_async(function, arguments):
    tpe = ThreadPoolExecutor()
    tasks = [_run_func_async(function, random_tile, tpe) for random_tile in arguments]
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(asyncio.gather(*tasks))


async def _run_func_async(
    func: Callable, func_args: Tuple[Any], executor: ThreadPoolExecutor
):
    if asyncio.iscoroutinefunction(func):
        return await func(*func_args)
    else:
        return await asyncio.get_event_loop().run_in_executor(
            func=lambda: func(*func_args), executor=executor
        )
