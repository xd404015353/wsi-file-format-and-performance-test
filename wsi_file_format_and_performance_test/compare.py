import glob
import json
import os

import requests

from wsi_file_format_and_performance_test.helpers import (
    get_data_path,
    get_reader_classes,
)


def main():
    filepaths = glob.glob(os.path.join(get_data_path(), "*.json"))
    for filepath in filepaths:
        for Reader in get_reader_classes():
            print(os.path.basename(filepath), Reader.get_reader_name())
            result_path = os.path.join(
                get_data_path(),
                "results",
                Reader.get_reader_name() + "-" + os.path.basename(filepath),
            )
            results = None
            if os.path.exists(result_path):
                with open(result_path) as f:
                    results = json.load(f)
            url = (
                "https://empaia.gitlab.io/integration/wsi-file-format-and-performance-test/results/"
                + Reader.get_reader_name()
                + "-"
                + os.path.basename(filepath)
            )
            r = requests.get(url)
            results_ref = None
            if r.status_code == 200:
                results_ref = r.json()
            if results and results_ref:
                for result_name in results_ref:
                    result_check = (
                        results_ref[result_name] == results[result_name]
                        or results[result_name]
                    )
                    if not result_check:
                        print(
                            "RESULT CHECK FAILED",
                            result_name,
                            "- reference:",
                            results_ref[result_name],
                            "- result:",
                            results[result_name],
                        )
                    assert (
                        results_ref[result_name] == results[result_name]
                        or results[result_name]
                    )
                    if "performance" in result_name:
                        change = round(
                            100.0
                            * (results_ref[result_name] - results[result_name])
                            / results_ref[result_name]
                        )
                        performance_check = change <= 20.0
                        if not performance_check:
                            print(
                                "PERFORMANCE DECREASE HIGHER THAN 20%",
                                "- change:",
                                str(
                                   change
                                )
                                + "%",
                                "- reference:",
                                results_ref[result_name],
                                "- result:",
                                results[result_name],
                            )
                        assert performance_check


if __name__ == "__main__":
    main()
